export class Details {

    firstName?:string;
    lastName?:string;
    email?:string;
    phone?:number;
    budget?:string;
    companyName?:string;
    id?:number;
}
