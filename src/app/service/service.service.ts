import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Details } from '../model/details';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private baseUrl='http://localhost:3000/details'

  constructor(private http: HttpClient) { }
  getById(id:number): Observable <any>{
    return this.http.get(`${this.baseUrl}/${id}`)

  }
  create(details:Details):Observable<object>{
    debugger
    return this.http.post(`${this.baseUrl}`,details)
  }

  getDetails(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
