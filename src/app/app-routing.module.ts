import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailsComponent } from './user-details/user-details.component';
import { ViewComponent } from './view/view.component';

const routes: Routes = [
  { path: '', component: UserDetailsComponent },
  { path: 'userDetails', component: UserDetailsComponent },
  { path: 'view', component: ViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
