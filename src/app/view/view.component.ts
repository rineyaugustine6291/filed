import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Details } from '../model/details';
import { ServiceService } from '../service/service.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  details: Observable<Details[]>;
  constructor(private service: ServiceService,private router: Router) { }

  ngOnInit() {
    debugger
    this.reloadData();
  }
  reloadData() {
    debugger
    this.details = this.service.getDetails();
  }

}
