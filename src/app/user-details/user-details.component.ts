import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Details } from '../model/details';
import { ServiceService } from '../service/service.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  form:FormGroup;
  details: Details=new Details();

  constructor(private service: ServiceService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit(): void {
    debugger;
    this.form= new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      companyName: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      budget: new FormControl('', Validators.required),
      
    })
    this.details=new Details();
  }
  onSubmit( ){
    debugger
    ;
    let formValue=this.form.value;
    let regObj={
      firstName:formValue.firstName,
      lastName:formValue.lastName,
      email:formValue.email,
      phone:formValue.phone,
      budget:formValue.budget,
      companyName:formValue.companyName,
  }
    this.save(regObj);
    
  }
  save(regObj){
    debugger
    this.service.create(regObj).subscribe(data=>{
      console.log(data)
      this.details= regObj;
      this.back();
    })
    
  }
  back(){
    debugger
    this.router.navigate(['view']);
  }

}
